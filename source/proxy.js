const http = require('http');
const https = require('https');
const fs = require('fs');
const path = require('path');
const url = require('url');

const STATIC_FILES_DIRECTORY = './assets'; // Replace this with your static files directory

// Function to serve static files
const serveStaticFile = (req, res) => {
  const parsedUrl = url.parse(req.url);
  const pathname = parsedUrl.pathname;

  const filePath = path.join(STATIC_FILES_DIRECTORY, pathname);

  // Check if the file exists
  if (!fs.existsSync(filePath) || fs.statSync(filePath).isDirectory()) {
    // If the file doesn't exist or is a directory, proceed with proxy
    return proxy(req, res);
  }

  // Set content type based on file extension
  const contentType = getContentType(filePath);
  res.setHeader('Content-Type', contentType);
  res.setHeader('Transfer-Encoding', 'chunked');

  // Create read stream for file and pipe it to response
  const fileStream = fs.createReadStream(filePath);
  fileStream.pipe(res);
};

// Function to determine content type based on file extension
const getContentType = (filePath) => {
  const ext = path.extname(filePath).toLowerCase();
  switch (ext) {
    case '.js':
      return 'text/javascript';
    case '.html':
      return 'text/html';
    default:
      return 'application/octet-stream';
  }
};
  
  
  

const proxy = (req, res) => {
  const { method, headers } = req;
  const targetUrl = req.url;

  
  // Log the incoming request
  console.log(`Incoming request: ${method} ${targetUrl}`);

  // Replace the origin with your target host

  const options = {
    hostname: 'pubads.g.doubleclick.net',
    port: 443,
    path: targetUrl, // Use req.url directly
    method: method,
    rejectUnauthorized: false,
    headers: {
      'accept': '*/*',
      'accept-language': 'en-US,en;q=0.9',
      'origin': 'https://10play.com.au',
      'referer': 'https://10play.com.au/im-a-celebrity-get-me-out-of-here/episodes/season-10/episode-6/tpv240331gwvjs',
      'sec-ch-ua': '"Brave";v="123", "Not:A-Brand";v="8", "Chromium";v="123"',
      'sec-ch-ua-mobile': '?0',
      'sec-ch-ua-platform': '"Windows"',
      'sec-fetch-dest': 'empty',
      'sec-fetch-mode': 'cors',
      'sec-fetch-site': 'cross-site',
      'sec-gpc': '1',
      'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36',
      'Connection': 'keep-alive', // Ensure persistent connection
    }
  };

  const proxyReq = https.request(options, (proxyRes) => {
    let responseBody = '';

    proxyRes.on('data', (chunk) => {
      responseBody += chunk;
    });
    proxyRes.on('end', () => {
      // Remove lines containing 'https://redirector.googlevideo.com/videoplayback/'
      responseBody = responseBody.split('\n').filter(line => !line.includes('https://redirector.googlevideo.com/videoplayback/')).join('\n');
      proxyRes.headers['access-control-allow-origin'] = '*';
      //console.log(`Proxy response status: ${proxyRes.statusCode}`);
      //console.log(`Proxy response headers:`, proxyRes.headers);
      // Set the appropriate headers
      res.writeHead(proxyRes.statusCode, proxyRes.headers);
      res.end(responseBody);
    });
  });

  proxyReq.on('error', (err) => {
    console.error('Proxy request error:', err);
    res.statusCode = 500;
    res.end('Proxy request error');
    
  });

  // Proxy the request body if any
  req.on('data', (chunk) => {
    proxyReq.write(chunk);
  });

  req.on('end', () => {
    proxyReq.end();
  });
};

// Create a simple HTTP server to serve static files or act as a reverse proxy
const server = http.createServer((req, res) => {
  serveStaticFile(req, res);
});

server.listen(8000, () => {
  console.log('Server listening on port 8000');
});

